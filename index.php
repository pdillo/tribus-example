<!DOCTYPE html>
<html>
	<head>
		<style>
		html,
		body {
			height: 100%;
			margin: 0;
		}
		#map {
			height: 100%;
			width: 100%;  
		}
		</style>
		<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/jquery-3.4.1.min.js"></script>
	</head>
	<body>
		<div id="map"></div>
		<script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDRwAWjcHl5SlU9pcAnQU8gWUJhUvdPh5c&callback=initMap"></script>
		<script>
		// Vars
		var locationObjs = [];
		function initMap() {
			// Init Vars
			var center = { lat: 39.7524792, lng: -105.0016463 };
			var options = {
				zoom: 15,
				center: center,
				mapTypeId: 'terrain'
			}
			var icons = {
				url: '/wp-content/themes/tribus/coffee-pin.png',
				scaledSize: new google.maps.Size(48, 48)
			};
			var map = new google.maps.Map(document.getElementById("map"), options);
			var infoWindows = [];
			// Load rest api data
			$.ajax({
				type: "GET",
				url: '/wp-json/wp/v2/locations?_fields=id,title,acf',
				success: function(data) {
					$.each(data, function(i, location) {
						var marker = new google.maps.Marker({
							map: map,
							position: getLatLng(
                                                 		location.acf.coordinates.latitude,
                                        			location.acf.coordinates.longitude
                                        		),
							title: location.title.rendered,
							icon: icons,
							id: location.id,
						});
						// Info window	
						const infoWindow = new google.maps.InfoWindow();
						infoWindows.push(infoWindow);
						marker.addListener('click', function() {
							for (var i=0;i<infoWindows.length;i++) {
								infoWindows[i].close();
							}
							const name = location.title.rendered;
							const description = location.acf.description;
							const hours = location.acf.hours;
							const phone = location.acf.phone;
							const position = getLatLng(location.acf.coordinates.latitude, location.acf.coordinates.longitude);
							const logo = location.acf.logo;
							const content = `
								<img style="float:left; width:200px; margin-top:30px" src="${logo}">
									<div style="margin-left:220px; margin-bottom:20px;">
										<h2>${name}</h2><p>${description}</p>
										<p><b>Open:</b> ${hours}<br/><b>Phone:</b> ${phone}</p>
										<p><img src="https://maps.googleapis.com/maps/api/streetview?size=350x120&location=${position.lat()},${position.lng()}&key=AIzaSyDRwAWjcHl5SlU9pcAnQU8gWUJhUvdPh5c"></p>
									</div>
							`;

							infoWindow.setContent(content);
							infoWindow.setPosition(position);
							infoWindow.open(map,this);

						});
					});
				},
				dataType: 'json'
			})
		} 
		// Helper functions
		function getLatLng(lat, lng) {
			var latLng = new window.google.maps.LatLng(lat, lng);
			return latLng;
		}
		</script>
	</body>
</html>
